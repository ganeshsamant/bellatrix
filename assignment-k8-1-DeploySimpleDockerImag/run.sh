#!/bin/sh
[[ "$1" = "" ]] && port=10080 || port=$1

read -p "-----Ensure that k8 setup is good. Check ../common/k8-start.sh. Stop instead?" answer
if [ "$answer" = "y" ]; then
	return
fi

read -p "-----Recreate Pod?" answer
if [ "$answer" = "y" ]; then
	echo "-----Creating Pod using yaml file\n"
	kubectl create -f bellatrix-pod.yaml
fi

echo "-----Check created Pod. Pod creation may take a long time\n"
while true
do
	kubectl describe pods bellatrix-pod
	read -p "-----Do you see the Pod listed?" answer
	case $answer in
		[yY]* ) break;;
   		* ) echo "----Will wait for 10 seconds"; sleep 10;;
	esac
done

echo "-----forward local port requests to the container's port. Run the following command in a separate shell\n"
echo "kubectl port-forward bellatrix-pod $port:8080"
read -p "-----Press any key to continue"

echo "-----Trying webapp : $PWD\n"
../common/tryUrl.sh 1 localhost $port
read -p "-----Press any key to continue"

echo "-----View Pod Logs. Can also use -f flag\n"
kubectl logs bellatrix-pod
read -p "-----Press any key to continue"

echo "-----Connect to the Pod shell and try ls -l /usr/local/tomcat/webapps/ command remember to exit the shell.\n"
kubectl exec bellatrix-pod --stdin --tty -c bellatrix-pod-container /bin/bash

echo "-----Stop port forward Shell\n"
read -p "-----Press any key to continue"

echo "-----Deleting Pod\n"
kubectl delete pods bellatrix-pod
