#!/bin/bash 

read -p "-----Rebuild Image ?" rebuildImage
if [ "$rebuildImage" = "y" ]; then

	echo "-----copying war file we created in previous assignment\n"
	cp -v ../assignment-1-SampleTomcatApp/bellatrix.war ./

	echo "-----Building docker image using the Dockerfile in this folder\n"
	docker build -t 'bellatrix' .

	echo "-----Tagging image\n"
	docker tag bellatrix hsenagtnamas/bellatrix:latest

	echo "-----Listing images\n"
	docker image ls

	echo "-----pushong image to docker cloud\n"
	docker push hsenagtnamas/bellatrix

	echo "-----pulling image from docker cloud\n"
	docker pull hsenagtnamas/bellatrix:latest
fi

echo "---checking docker image\n"
docker image ls bellatrix

echo "-----Starting Docker container using docker image"
docker run --rm -d -p 8080:8080 bellatrix

echo "-----Trying webapp : $PWD\n"
../common/tryUrl.sh

read -p "-----Press any key to continue"

echo "-----Stopping all docker containers\n"
docker container stop $(docker ps -a -q)


