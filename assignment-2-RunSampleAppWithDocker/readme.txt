Goal : Run a docker instance of the webapp we developed in previous assignment


Steps : 

1. Webapp in assignment 1 is ready
2. Write appropriate Dockerfile
3. Deploy : copy sample.war file to dockers webapps folder
4. Test : 
	. docker run -rm -d -p 8080:8080 bellatrix
	. http://localhost:8080/sample/
	. docker container ls
	. docker container stop
	. docker container rm
5. Push the docker image to repository for reuse
	. docker login
	. docker tag bellatrix hsenagtnamas/bellatrix:latest
	. docker image ls
	. docker push hsenagtnamas/bellatrix
	. docker pull hsenagtnamas/bellatrix:latest
