#!/bin/sh 
[[ "$1" = "" ]] && maxAttempts=1 || maxAttempts=$1

[[ "$2" = "" ]] && ip='localhost' || ip=$2
[[ "$3" = "" ]] && port=8080 || port=$3

url="http://$ip:$port/bellatrix/addNumbers?num1=1&num2=2"

echo '-----Waiting for webapp to get ready\n'
until [ "`curl --silent --show-error --connect-timeout 1 $url | grep -i 'Bellatrix'`" != "" ];
do
  echo '\t--- sleeping for 10 seconds\n'
  sleep 10
done

echo '-----Trying Webapp\n'

for ((attempt=1; attempt<=maxAttempts; attempt++)); do
	num1=$((1 + RANDOM % 100))
	num2=$((1 + RANDOM % 100))
	echo "\n----Attempt : $attempt----\n"
	url="http://$ip:$port/bellatrix/addNumbers?num1=$num1&num2=$num2"; 
	echo curl $url
	curl $url
done

echo '\n-----\n'
