#!/bin/sh

echo "----Checking minukube\n"
minikube status
read -p "-----Start minikube?" answer
if [ "$answer" = "y" ]; then

	echo "----Starting minikube\n"
	minikube start --vm-driver=xhyve

fi

echo "----linking k8 and minukube\n"
kubectl config use-context minikube

echo "----Checking cluster info\n"
kubectl cluster-info

echo "----Run kubectl proxy in a separate terminal\n"
read -p "-----Press any key to continue"

echo "---Checking kubectl proxy\n"
curl http://localhost:8001/version
read -p "-----Press any key to continue"

echo "----\n"
minikube dashboard
