#!/bin/bash 
[[ "$1" = "" ]] && replicas=3 || replicas=$1
[[ "$2" = "" ]] && host='bellatrix-web-service-bellatrix.193b.starter-ca-central-1.openshiftapps.com' || host=$2
[[ "$3" = "" ]] && port=80 || port=$3

echo "----Prepare Openshift web - See Readme\n"
read -p "-----Press any key to continue"

../common/tryUrl.sh $replicas $host $port

