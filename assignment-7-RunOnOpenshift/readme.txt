Goal : Run the Docker image on OpenShift web

Steps : 

1. Docker image in assignment 6 is ready and is pushed to registry ( and is public )
2. Openshift
	. Use cloud instance on openshift
	. Login
	. Add yaml files in this folder ( Deployment and Service )
	. Create a route to make the service accessible to external world
3. Test : ./run.sh
