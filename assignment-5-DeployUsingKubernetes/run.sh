#~/bin/bash
[[ "$1" = "" ]] && replicas=10 || replicas=$1
minikubeip=`minikube ip`

read -p "-----Ensure that k8 setup is good. Check ../common/k8-start.sh. Stop instead?" answer
if [ "$answer" = "y" ]; then
        return
fi

echo "-----deploying bellatrix docker\n"
kubectl create -f bellatrix.yaml
kubectl get deployments
read -p "-----Press any key to continue"


echo "---Create Bellatrix service\n"
kubectl create -f bellatrix-service.yaml
read -p "-----Press any key to continue"

echo "---Create Load Balancer\n"
kubectl create -f bellatrix-ingress.yaml
read -p "-----Press any key to continue"

echo "----Check deployments\n"
kubectl get deployments
read -p "-----Press any key to continue"

echo "----Check services\n"
kubectl get services
read -p "-----Press any key to continue"

echo "----Check replicasets\n"
kubectl get replicasets
read -p "-----Press any key to continue"

echo "----Check replicasets\n"
kubectl get pods
read -p "-----Press any key to continue"

#echo "---Get mikube IP\n"
#minicube ip
#echo "---save hostname specified in ingress file ( bellatrix.web )  with above ip in hosts file by running following command\n"
#echo "sudo vi /etc/hosts"
##192.168.64.4    bellatrix.web
#read -p "-----Press any key to continue"

echo "-----Trying deployment : nginx should now also talk to these containers : $PWD\n"
../common/tryUrl.sh $replicas $minikubeip 31000
read -p "-----Press any key to continue"

echo "----Update replicasets in bellatrix.yaml\n"
read -p "-----Press any key to continue"
kubectl apply -f bellatrix.yaml

echo "-----Trying scaled webapp : nginx should now also talk to new containers : $PWD\n"
#../common/tryUrl.sh $replicas bellatrix.web 80
../common/tryUrl.sh $replicas $minikubeip 31000
read -p "-----Press any key to continue"

echo "-----Removing ingress"
kubectl delete ingress bellatrix-web-ingress

echo "-----Removing Service"
kubectl delete services bellatrix-web-service

echo "-----Removing Deployments"
kubectl delete deployments bellatrixcluster


