#!/bin/sh
echo "-----checking CATALINA_HOME variable\n"
: "${CATALINA_HOME:? Please set CATALINA_HOME to Tomcat root folder}"

echo "-----Copying war file\n"
cp bellatrix.war $CATALINA_HOME/webapps

echo "-----Starting Tomcat\n"
$CATALINA_HOME/bin/startup.sh

echo "-----Trying webapp : $PWD\n"
../common/tryUrl.sh

echo "-----Stopping Tomcat\n"
$CATALINA_HOME/bin/shutdown.sh

