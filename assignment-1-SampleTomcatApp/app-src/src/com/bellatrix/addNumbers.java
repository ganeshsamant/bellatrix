package com.bellatrix;

import java.io.IOException;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.util.Enumeration;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class addNumbers
 */
@WebServlet("/addNumbers")
public class addNumbers extends HttpServlet {
	private static final long serialVersionUID = 1L;
    private static final String myIpAddress =   getLocalHostLANAddress();
    /**
     * @see HttpServlet#HttpServlet()
     */
    public addNumbers() {
        super();
        
    }
    
    

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String res = "";
		try {
			int num1 = Integer.parseInt(request.getParameter("num1"));
			int num2 = Integer.parseInt(request.getParameter("num2"));
			
			int sum = num1 + num2;
			res = "" + num1 + " + " + num2 + " = " + sum ; 
		}catch(Exception e) {
			res = "Please specify 'num1' & 'num2' in query parameters.";
		}
		
		response.getWriter().append(
				"<h1>Bellatrix AddNumber Service is Up</h1><br/>"
				+ "<h2>" +  res + "</h2><br/>"
				+ "Served by IP Address : " + myIpAddress + "<br/>");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		doGet(request, response);
	}

	
	private static String getLocalHostLANAddress()  {
		String ipAddresses = "{";
	    try {
	        //InetAddress candidateAddress = null;
	        // Iterate all NICs (network interface cards)...
	        for (Enumeration ifaces = NetworkInterface.getNetworkInterfaces(); ifaces.hasMoreElements();) {
	            NetworkInterface iface = (NetworkInterface) ifaces.nextElement();
	            // Iterate all IP addresses assigned to each card...
	            ipAddresses += iface.getDisplayName() + ":[";
	            for (Enumeration inetAddrs = iface.getInetAddresses(); inetAddrs.hasMoreElements();) {
	                InetAddress inetAddr = (InetAddress) inetAddrs.nextElement();
	                ipAddresses += inetAddr.toString()+ ";";
	                
	                
	            }
	            ipAddresses += "],";
	        }

	        
	        // At this point, we did not find a non-loopback address.
	        // Fall back to returning whatever InetAddress.getLocalHost() returns...
	        InetAddress jdkSuppliedAddress = InetAddress.getLocalHost();
	        if (jdkSuppliedAddress != null) {
	        	ipAddresses += "def:[" + jdkSuppliedAddress.toString()+ "]";
	        }
	        ipAddresses += "}";
	        return ipAddresses;
	    }
	    catch (Exception e) {
	        return "Unknown IP";
	    }
	}

}
