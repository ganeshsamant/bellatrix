Goal : Scale app using load balancer provided by Docker framework


Steps : 

1. Bellatrix docker image is pushed to docker registry
2. Optional - Write docker yaml file, specify instances and network
3. Initialize swarm - docker swarm init
4. Test : ./run.sh
