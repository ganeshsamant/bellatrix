#!/bin/sh
[[ "$1" = "" ]] && replicas=3 || replicas=$1
[[ "$2" = "" ]] && scale=4 || scale=$2

#!/bin/bash 
echo "-----Initializing Swarm\n"
docker swarm init

echo "-----reating overlay network\n"
docker network create --driver overlay bellatrix_network

echo "-----Deploying Service\n"
#docker stack deploy -c docker-compose.yml bellatrixSwarm
docker service create --name bellatrix_service --hostname bellatrix.web --replicas $replicas --publish 8080:8080 --network bellatrix_network hsenagtnamas/bellatrix:latest

echo "-----List of services\n"
docker service ls

echo "-----Our Service\n"
echo "----We should see $replicas instances below\n"
docker service ps bellatrix_service

echo "-----Our Containers\n"
docker container ls

echo "-----Trying webapp : $PWD\n"
../common/tryUrl.sh $replicas

read -p "-----Press any key to continue"

echo "----scaling service:$scale\n"
docker service scale bellatrix_service=$scale

echo "----We should see $scale instances below\n"
docker service ps bellatrix_service

echo "-----Trying scales webapp : $PWD\n"
../common/tryUrl.sh $scale

read -p "-----Press any key to continue"

echo "-----Removing Service\n"
docker service rm bellatrix_service

echo "-----Removing Bellatrix Service\n"
docker network rm bellatrix_network

echo "-----List of services should be 0\n"
docker service ls

echo "-----Our Containers should be 0\n"
docker container ls

echo "-----Leaving Swarm\n"
docker swarm leave --force

