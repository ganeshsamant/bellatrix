#!/bin/sh
[[ "$1" = "" ]] && port=31000 || port=$1
minikubeip=`minikube ip`

read -p "-----Ensure that k8 setup is good. Check ../common/k8-start.sh. Stop instead?" answer
if [ "$answer" = "y" ]; then
	return
fi

read -p "-----Recreate Pod?" answer
if [ "$answer" = "y" ]; then
	echo "-----Creating Pod using yaml file\n"
	kubectl create -f bellatrix-pod.yaml
fi

echo "-----Check created Pod. Pod creation may take a long time\n"
while true
do
	kubectl describe pods bellatrix-pod
	read -p "-----Do you see the Pod Started?" answer
	case $answer in
		[yY]* ) break;;
   		* ) echo "----Will wait for 10 seconds"; sleep 10;;
	esac
done


read -p "-----Recreate Service?" answer
if [ "$answer" = "y" ]; then
	echo "-----Creating Service using -service.yaml file\n"
	kubectl create -f bellatrix-web-service-for-pod.yaml
	read -p "-----Recreate Service?" answer
fi
kubectl describe service bellatrix-web-service-for-pod

echo "----Check services\n"
kubectl get services

echo "-----Trying webapp : $PWD\n"
../common/tryUrl.sh 1 $minikubeip $port
read -p "-----Press any key to continue"

echo "-----Deleting Service\n"
kubectl delete services bellatrix-web-service-for-pod

echo "-----Deleting Pod\n"
kubectl delete pods bellatrix-pod
