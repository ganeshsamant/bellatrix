#!/bin/bash
[[ "$1" = "" ]] && replicas=3 || replicas=$1
[[ "$2" = "" ]] && scale=4 || scale=$2

read -p "-----Rebuild nginx Image ?" rebuildImage
if [ "$rebuildImage" = "y" ]; then

        echo "-----Building docker image using the Dockerfile in this folder\n"
	docker build -t 'bellatrix-nginx' .

        echo "-----Tagging image\n"
	docker tag bellatrix-nginx hsenagtnamas/bellatrix-nginx:latest

        echo "-----Listing images\n"
        docker image ls

        echo "-----pushong image to docker cloud\n"
	docker push hsenagtnamas/bellatrix-nginx:latest

        echo "-----pulling image from docker cloud\n"
	docker pull hsenagtnamas/bellatrix-nginx:latest
fi


echo "-----Initializing Swarm\n"
docker swarm init

echo "-----Creating overlay network\n"
docker network create --driver overlay bellatrix_network

echo "-----list networks\n"
docker network ls

echo "-----Deploying Bellatrix Service with created network\n"
#docker stack deploy -c docker-compose.yml bellatrixSwarm
docker service create --name bellatrix_service --replicas $replicas --publish 8080:8080 --network bellatrix_network hsenagtnamas/bellatrix:latest

echo "-----Deploying Bellatrix-nginx Service to the same network\n"
docker service create --name bellatrix_nginx_service --replicas 1 -p 8090:80 -p 9443:443 --network bellatrix_network hsenagtnamas/bellatrix-nginx:latest

echo "-----Listing services\n"
docker service ls

echo "-----Listing Containers\n"
docker container ls

echo "-----Trying webapp : $PWD\n"
../common/tryUrl.sh $replicas localhost 8090
read -p "-----Press any key to continue"

echo "----scaling service:$scale\n"
docker service scale bellatrix_service=$scale

echo "----We should see $scale instances below\n"
docker service ps bellatrix_service

echo "-----Trying scaled webapp : nginx should now also talk to new containers : $PWD\n"
../common/tryUrl.sh $scale
read -p "-----Press any key to continue"

echo "-----Removing Bellatrix Service\n"
docker service rm bellatrix_service

echo "-----Removing Bellatrix_nginx Service\n"
docker service rm bellatrix_nginx_service

echo "-----Removing Bellatrix Service\n"
docker network rm bellatrix_network

echo "-----List of services should be 0\n"
docker service ls

echo "-----Our Containers should be 0\n"
docker container ls

echo "-----Leaving Swarm\n"
docker swarm leave --force

