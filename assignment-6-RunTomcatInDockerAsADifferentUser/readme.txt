Goal : Create a custom docker image to allow tomcat to run on openshift web

Steps : 

1. Webapp in assignment 1 is ready
2. Write appropriate Dockerfile ( note that tomcat runs a root, so we need to customize it, see https://docs.openshift.com/online/creating_images/guidelines.html )
3. Test : ./run.sh
