#!/bin/bash 

read -p "-----Rebuild Image ?" rebuildImage
if [ "$rebuildImage" = "y" ]; then

	echo "-----copying war file we created in previous assignment\n"
	cp -v ../assignment-1-SampleTomcatApp/bellatrix.war ./

	echo "-----Building docker image using the Dockerfile in this folder\n"
	docker build -t 'regal' .

	echo "-----Tagging image\n"
	docker tag regal hsenagtnamas/regal:latest

	echo "-----Listing images\n"
	docker image ls

	echo "-----pushong image to docker cloud\n"
	docker push hsenagtnamas/regal

	echo "-----pulling image from docker cloud\n"
	docker pull hsenagtnamas/regal:latest
fi

echo "---checking docker image\n"
docker image ls regal

echo "-----Starting Docker container using docker image"
docker run --rm -d -p 8080:8080 regal

echo "-----Trying webapp : $PWD\n"
../common/tryUrl.sh

read -p "-----Press any key to continue"

echo "-----Stopping all docker containers\n"
docker container stop $(docker ps -a -q)


